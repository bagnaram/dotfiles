#!/usr/bin/env sh
set -ex

set -uo pipefail
scheme=$(darkman get)

set_rofi_theme()
{
    CDIR="${XDG_CONFIG_HOME:-${HOME}/.config}/rofi"
    if [ ! -d "${CDIR}" ]
    then
        mkdir -p "${CDIR}"
    fi
    get_link=$(readlink -f "${CDIR}/config.rasi")
    sed -i 's#@theme ".*"#@theme "'"$1"'"#g' "${get_link}" > /dev/null
}

set_mutt_theme()
{
    CDIR="${HOME}/.mutt"
    get_link=$(readlink -f "${CDIR}/muttrc")
    sed -i "s#source ~/dotfiles/mutt/themes/.*#source ~/dotfiles/mutt/themes/$1#g" "${get_link}" > /dev/null
}

set_daytime()
{
    #fyi -a "day-night-cycle" "☀ Switching to daytime"
    export icon_theme="breeze"
    gsettings set org.gnome.desktop.interface gtk-theme 'Breeze'
    gsettings set org.gnome.desktop.interface icon-theme $icon_theme
    gsettings set org.gnome.desktop.interface color-scheme 'prefer-light'
    crudini --set /home/bagnaram/.config/qt6ct/qt6ct.conf Appearance custom_palette false
    crudini --set /home/bagnaram/.config/qt6ct/qt6ct.conf Appearance icon_theme breeze

    #crudini --inplace --set --existing ~/.config/qt5ct/qt5ct.conf Appearance style Windows

    #crudini --inplace --set --existing ~/.config/qt5ct/qt5ct.conf Appearance icon_theme $icon_theme
    if pgrep "emacs" >/dev/null 2>&1 ; then
      emacsclient --eval '(light-theme)' --suppress-output &
    fi

    #belfonte-day theme
    export color0="20111b" \
           color1="be100e" \
           color2="858162" \
           color3="eaa549" \
           color4="426a79" \
           color5="97522c" \
           color6="989a9c" \
           color7="968c83" \
           color8="5e5252" \
           color9="be100e" \
           color10="858162" \
           color11="eaa549" \
           color12="426a79" \
           color13="97522c" \
           color14="989a9c" \
           color15="d5ccba" \
           color16="45373c" \
           color17="d5ccba"


    # set the swaylock theme
    envsubst < ~/.config/swaylock/config.env > ~/.config/swaylock/config
    envsubst < ~/.config/fnott/fnott.ini.env > ~/.config/fnott/fnott.ini
    envsubst < ~/.config/fuzzel/fuzzel.ini.env > ~/.config/fuzzel/fuzzel.ini
    #envsubst < ~/.config/mako/config.env > ~/.config/mako/config
    #makoctl reload

    # Basic color configuration using the Base16 variables for windows and borders.
    #        Property Name           Border  BG      Text    Indicator Child Border
    swaymsg "client.focused          $color17 $color17 $color16 $color12 $color17;
            client.focused_inactive $color6 $color6 $color5 $color3 $color6;
            client.unfocused        $color6 $color6 $color5 $color6 $color6;
            client.urgent           $color8 $color8 $color16 $color8 $color8;
            client.placeholder      $color6 $color6 $color5 $color6 $color6;
            client.background       $color7" &

    #set_rofi_theme /usr/share/rofi/themes/paper-float.rasi
    # set_mutt_theme mutt-colors-solarized-light-256.muttrc
    #pkill -RTMIN+1 waybar
    pkill -USR1 zsh
    pkill -HUP fnott
}

set_nighttime()
{
    #fyi -a "day-night-cycle" "🌙 Switching to nighttime"
    export icon_theme="breeze-dark"
	scheme=$(gsettings get org.gnome.desktop.interface color-scheme)
	if [ "$scheme" != "'prefer-dark'" ]; then
	gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
	fi
    gsettings set org.gnome.desktop.interface gtk-theme 'Breeze-Dark'
    gsettings set org.gnome.desktop.interface icon-theme $icon_theme
    gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
    crudini --set /home/bagnaram/.config/qt6ct/qt6ct.conf Appearance custom_palette true
    crudini --set /home/bagnaram/.config/qt6ct/qt6ct.conf Appearance icon_theme breeze-dark

    #crudini --inplace --set --existing ~/.config/qt5ct/qt5ct.conf Appearance style Adwaita-Dark
    #crudini --inplace --set --existing ~/.config/qt5ct/qt5ct.conf Appearance icon_theme $icon_theme

    #crudini --inplace --set --existing ~/.config/fnott/fnott.ini "" icon-theme $icon_theme

    if pgrep "emacs" >/dev/null 2>&1 ; then
      emacsclient --eval '(dark-theme)' --suppress-output &
    fi
    #kitty @ --to unix:/tmp/mykitty set-colors --all --configured ~/.config/kitty/dracula.conf &
    #yq eval -i '.colors alias = "dark"' ${HOME}/dotfiles/alacritty/dracula-pro.yml &
    # set sway theme
    export color0="000000" \
           color1="ff5555" \
           color2="50fa7b" \
           color3="ffe46c" \
           color4="bd93f9" \
           color5="ff79c6" \
           color6="8be9fd" \
           color7="bfbfbf" \
           color8="4d4d4d" \
           color9="ff6e67" \
           color10="5af78e" \
           color11="f4f99d" \
           color12="caa9fa" \
           color13="ff92d0" \
           color14="9aedfe" \
           color15="e6e6e6" \
           color16="f8f8f2" \
           color17="373949"

    # set the swaylock theme
    envsubst < ~/.config/swaylock/config.env > ~/.config/swaylock/config
    envsubst < ~/.config/fnott/fnott.ini.env > ~/.config/fnott/fnott.ini
    envsubst < ~/.config/fuzzel/fuzzel.ini.env > ~/.config/fuzzel/fuzzel.ini
    #envsubst < ~/.config/mako/config.env > ~/.config/mako/config
    #makoctl reload

    # Basic color configuration using the Base16 variables for windows and borders.
    #        Property Name           Border  BG      Text    Indicator Child Border
    swaymsg "client.focused          $color6 $color6 $color8 $color12 $color6;
             client.focused_inactive $color17 $color17 $color7 $color3 $color17;
             client.unfocused        $color17 $color17 $color7 $color6 $color17;
             client.urgent           $color8 $color8 $color16 $color8 $color8;
             client.placeholder      $color6 $color6 $color5 $color6 $color6;
             client.background       $color7" &

    #set_rofi_theme /usr/share/rofi/themes/purple.rasi
    # set_mutt_theme dracula.muttrc
    #pkill -RTMIN+1 waybar
    pkill -USR1 zsh
    pkill -HUP fnott
}

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit
fi

# Handle invocation as gammastep hook
case "$1" in
    "period-changed")
        # Test some echos
        #fyi -a "gammastep" "gammastep period change at $(date "+%H:%M")" "$(echo "$@")"
        if [ "$2" = "daytime" ] && [ "$3" = "transition" ]; then
            scheme="1"
            set_nighttime
        elif [ "$2" = "none" ] && [ "$3" = "transition" ]; then
            scheme="1"
            set_nighttime
        elif [ "$2" = "daytime" ] && [ "$3" = "night" ]; then
            scheme="1"
            set_nighttime
        elif [ "$2" = "night" ] && [ "$3" = "daytime" ]; then
            scheme="0"
            set_daytime
        elif [ "$2" = "transition" ] && [ "$3" = "daytime" ]; then
            scheme="0"
            set_daytime
        elif [ "$3" = "daytime" ]; then
            scheme="0"
            set_daytime
        elif [ "$3" = "none" ]; then
            scheme="0"
            set_daytime
        fi
    ;;
    "toggle")
        if [[ "$scheme" = "light" ]]; then
        sky=""
        printf '%s\nNight Mode\nnight' "$sky"
        set_daytime
        elif [[ "$scheme" = "dark" ]]; then
        sky=""
        printf '%s\nDay Mode\nday' "$sky"
        set_nighttime
        fi
    ;;
    "update")
        if [[ "$scheme" = "light" ]]; then
        sky=""
        printf '%s\nNight Mode\nnight' "$sky"
        set_nighttime
        elif [[ "$scheme" = "dark" ]]; then
        sky=""
        printf '%s\nDay Mode\nday' "$sky"
        set_daytime
        fi
    ;;
    "get")
        if [[ "$scheme" = "light" ]]; then
        sky=""
        printf '%s\nNight Mode\nnight' "$sky"
        elif [[ "$scheme" = "dark" ]]; then
        sky=""
        printf '%s\nDay Mode\nday' "$sky"
        fi
        exit
    ;;
    *)
        echo "Invalid option: $1"
    ;;
esac
