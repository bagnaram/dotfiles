set guicursor="disable"

" Tab settings
set shiftwidth=2        " Tab spacing of 2 columns
set softtabstop=2       " unify
set shiftround          " round to nearest tab
set expandtab           " expand tabs to spaces
set cursorline

" Draw settings
set lazyredraw
set showmatch

set cindent		
" smart indentation

hi CursorLine ctermbg=8 ctermfg=15 "8 = dark gray, 15 = white

" Dynamic incremental searching
set incsearch hlsearch
syntax on

" set termguicolors
"colo desert

let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" Obligatory
set nu

" Do Java autocomplete
autocmd Filetype java setlocal omnifunc=javacomplete#Complete

" VIM Pathogen plugin
execute pathogen#infect()

