DOTFILES := $(shell pwd)
WHOAMI := $(shell whoami)
SHELL := /bin/bash

.PHONY: all
all: mutt xterm vim i3 screen redshift dunst doom waybar rofi kanshi fnott swaylock fuzzel zsh hyprland

.PHONY: mutt
mutt:
	if [ ! -d "${HOME}/.mutt" ]; then ln -fns $(DOTFILES)/mutt ${HOME}/.mutt; fi

.PHONY: xterm
xterm:
	ln -fns $(DOTFILES)/Xdefaults ${HOME}/.Xdefaults
.PHONY: screen
screen:
	ln -fns $(DOTFILES)/screenrc ${HOME}/.screenrc
.PHONY: doom
doom:
	if [ ! -d "${HOME}/.doom.d" ]; then ln -fns $(DOTFILES)/.doom.d ${HOME}/.doom.d; fi
.PHONY: gammastep
gammastep:
	if [ ! -d "${HOME}/.config/gammastep" ]; then ln -fns $(DOTFILES)/gammastep ${HOME}/.config/gammastep; fi
.PHONY: vim
vim:
	if [ ! -d "${HOME}/.vim" ]; then ln -fns $(DOTFILES)/vim ${HOME}/.vim; fi
	if [ ! -d "${HOME}/.vim/autoload" ]; then ln -fns $(DOTFILES)/vim/pathogen/autoload ${HOME}/.vim/autoload; fi
.PHONY: nvim
nvim:
	if [ ! -d "${HOME}/.config/nvim" ]; then ln -fns $(DOTFILES)/nvim ${HOME}/.config/nvim; fi
.PHONY: nvim-pathogen
nvim-pathogen:
	git submodule update --init --recursive
	git submodule update --recursive --remote
	$(MAKE) -C nvim/bundle/telescope-fzf-native.nvim/

.PHONY: gpg-agent
gpg-agent:
	if [ ! -d "${HOME}/.gnupg/gpg-agent.conf" ]; then ln -fns $(DOTFILES)/gpg-agent.conf ${HOME}/.gnupg/gpg-agent.conf; fi
.PHONY: redshift
redshift:
	if [ ! -d "${HOME}/.config/redshift.conf" ]; then ln -fns $(DOTFILES)/redshift.conf ${HOME}/.config/redshift.conf; fi
.PHONY: i3
i3:
	mkdir -p ${HOME}/.config/i3/
	ln -fns $(DOTFILES)/sway/config  ${HOME}/.config/i3/config
	ln -fns $(DOTFILES)/sway/i3status.conf  ${HOME}/.config/i3/status.conf
.PHONY: hyprland
hyprland:
	if [ ! -d "${HOME}/.config/hypr" ]; then ln -fns $(DOTFILES)/hypr  ${HOME}/.config/hypr; fi
.PHONY: sway
sway:
	if [ ! -d "${HOME}/.config/i3" ]; then ln -fns $(DOTFILES)/sway  ${HOME}/.config/i3; fi
.PHONY: dunst
dunst:
	mkdir -p ${HOME}/.config/dunst/
	ln -fns $(DOTFILES)/dunstrc  ${HOME}/.config/dunst/dunstrc
.PHONY: kanshi
kanshi:
	if [ ! -d "${HOME}/.config/kanshi" ]; then ln -fns $(DOTFILES)/kanshi  ${HOME}/.config/kanshi; fi
.PHONY: waybar
waybar: scripts
	ln -fns $(DOTFILES)/waybar  ${HOME}/.config/waybar
.PHONY: pavolume
pavolume:
	ln -fns $(DOTFILES)/pavolume  ${HOME}/.config/pavolume
.PHONY: mako
mako:
	ln -fns $(DOTFILES)/mako  ${HOME}/.config/mako
	. mako/.env && envsubst < ${HOME}/.config/mako/config.env > ${HOME}/.config/mako/config
.PHONY: fnott
fnott:
	ln -fns $(DOTFILES)/fnott  ${HOME}/.config/fnott
.PHONY: fuzzel
fuzzel:
	ln -fns $(DOTFILES)/fuzzel  ${HOME}/.config/fuzzel
.PHONY: fontconfig
fontconfig:
	ln -fns $(DOTFILES)/fontconfig  ${HOME}/.config/fontconfig
.PHONY: swaylock
swaylock:
	ln -fns $(DOTFILES)/swaylock  ${HOME}/.config/swaylock
	. swaylock/.env && envsubst < ${HOME}/.config/swaylock/config.env > ${HOME}/.config/swaylock/config
.PHONY: scripts
scripts:
	ln -fns $(DOTFILES)/scripts  ${HOME}/.config/scripts
.PHONY: yambar
yambar:
	ln -fns $(DOTFILES)/yambar  ${HOME}/.config/yambar
.PHONY: rofi
rofi:
	mkdir -p ${HOME}/.config/rofi/
	ln -fns $(DOTFILES)/rofi/config.rasi  ${HOME}/.config/rofi
.PHONY: alacritty
alacritty:
	mkdir -p ${HOME}/.config/alacritty/
	touch ${HOME}/.config/alacritty/alacritty.yml
	yq w -i ${HOME}/.config/alacritty/alacritty.yml import[+] $(DOTFILES)/alacritty/dracula.yml
.PHONY: foot
foot:
	if [ ! -d "${HOME}/.config/foot" ]; then ln -fns $(DOTFILES)/foot ${HOME}/.config/foot; fi
.PHONY: darkman
darkman:
	mkdir -p ${HOME}/.config/darkman/
	mkdir -p ${HOME}/.local/share/light-mode.d
	mkdir -p ${HOME}/.local/share/dark-mode.d
	ln -fns $(DOTFILES)/darkman/config.yaml  ${HOME}/.config/darkman/config.yaml
	ln -fns $(DOTFILES)/darkman/light.sh  ${HOME}/.local/share/light-mode.d/light.sh
	ln -fns $(DOTFILES)/darkman/dark.sh  ${HOME}/.local/share/dark-mode.d/dark.sh
.PHONY: crontab
crontab:
	sed -i "s#DOTFILES#${DOTFILES}#g" ${DOTFILES}/cron/crontab
	(crontab -l ; cat ${DOTFILES}/cron/crontab)| crontab -
.PHONY: systemd
systemd:
	mkdir -p ${HOME}/.config/systemd/
	if [ ! -d "${HOME}/.config/systemd/user.control/" ]; then ln -fns $(DOTFILES)/systemd ${HOME}/.config/systemd/user.control; fi
	systemctl --user enable battery@$(WHOAMI).service
	systemctl --user start battery@$(WHOAMI).service
	systemctl --user enable battery@$(WHOAMI).timer
	systemctl --user start battery@$(WHOAMI).timer
.PHONY: clean
.PHONY: zsh
zsh:
	ln -fns $(DOTFILES)/zshrc  ${HOME}/.zshrc
clean:
	rm ${HOME}/.muttrc
	rm ${HOME}/.mailcap
	rm ${HOME}/.Xdefaults
	rm ${HOME}/.vim/autoload/
	rm ${HOME}/.vim/
	rm ${HOME}/.config/i3/config
	rm ${HOME}/.config/i3/status.conf
	rm ${HOME}/.config/fuzzel/fuzzel.ini
	rm ${HOME}/.zshrc
	rm ${HOME}/.doom.d
	rm ${HOME}/.config/yambar
