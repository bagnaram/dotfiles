local wk = require("which-key")
local builtin =  require("telescope.builtin")


wk.add({
  { "<leader>f", group = "file" }, -- group
  { "<leader>ff", "<cmd>Telescope find_files<cr>", desc = "Find File", mode = "n" },
  { "<leader>fr", "<cmd>Telescope oldfiles sort_lastused=true ignore_current_buffer=true <cr>", desc = "Find Recent", mode = "n" },
  { "<leader>fn", desc = "New File" },
  { "<leader>f1", hidden = true }, -- hide this keymap
  {
    -- Nested mappings are allowed and can be added in any order
    -- Most attributes can be inherited or overridden on any level
    -- There's no limit to the depth of nesting
    mode = { "n", "v" }, -- NORMAL and VISUAL mode
    { "<leader>q", "<cmd>q<cr>", desc = "Quit" }, -- no need to specify mode since it's inherited
    { "<leader>w", "<cmd>w<cr>", desc = "Write" },
  },
  { "<leader>g", icon = "", desc = "Neogit"},
  { "<leader>gg", desc = "󱖫  status"},
  { "<leader>p", icon = "", group = "Project"},
  { "<leader>pp", desc = "  projects"},
  { "<leader>ps", function() builtin.grep_string({
      search = vim.fn.input("Grep > " ) }) end, icon = "", desc = "search"},
  { "<leader>s", group = "Search", icon = ""},
  { "<leader>sf", "<cmd>Telescope find_files hidden=true<cr>", icon = "", desc = "Search File Names"},
  { "<leader>sp", "<cmd>Telescope live_grep hidden=true<cr>", icon = "", desc = "Search Project"},
  { "<leader>b", icon = "", group = "Buffers"},
  { "<leader>bb", desc = " list"},
  { "<leader>bk", icon = "❌", group = "kill"},
  { "<leader>o", group = "Open"},
  { "<leader>op", icon = "", group = "projects"},
  { "<leader>y", desc = "yank to buffer"},
  { "<leader>Y", desc = "yank to system buffer"},
  { "<leader><", desc = "buffers"}


})


