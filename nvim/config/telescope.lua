-- Mapping <Esc> to quit in insert mode
local actions = require("telescope.actions")

require("telescope").setup{
  mappings = {
    i = {
      ["<esc>"] = actions.close
    },
  },
}
-- require('telescope').load_extension('projects')
-- To get fzf loaded and working with telescope, you need to call
-- load_extension, somewhere after setup function:
require('telescope').load_extension('fzf')
-- sort telescope buffers by last used

