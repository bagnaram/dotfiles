vim.cmd.colorscheme "catppuccin"

require 'darkman'.setup{
  change_background = true,
  send_user_event = false,
  colorscheme = { dark = "catppuccin-frappe", light = "catppuccin-latte" }
}
