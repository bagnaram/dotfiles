require('project_nvim').setup{
  ignore_lsp = { "helm_ls", "terraformls" },
  exclude_dirs = { ".chart", ".charts"},
  patterns = { ".git", "_darcs", ".hg", ".bzr", ".svn", "package.json" },

}
