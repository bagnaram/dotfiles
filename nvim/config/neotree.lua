require("neo-tree").setup {
  default_component_configs = {
    git_status = {
      name = {
        trailing_slash = false,
        use_git_status_colors = true,
        highlight = "NeoTreeFileName",
      },
      symbols = {
        -- Change type
        added     = "", -- or "✚", but this is redundant info if you use git_status_colors on the name
        modified  = "✏", -- or "", but this is redundant info if you use git_status_colors on the name
        deleted   = "❌",-- this can only be used in the git_status source
        renamed   = "🆎",-- this can only be used in the git_status source
        -- Status type
        untracked = "❓",
        ignored   = "🔂",
        unstaged  = "💡",
        staged    = "✅",
        conflict  = "",
      }
    }
  },
  window = {
    position = "left",
  },
  filesystem = {
    filtered_items = {
      hide_dotfiles = false,
    },
    follow_current_file = {
      enabled = true, -- This will find and focus the file in the active buffer every time
      --               -- the current file is changed while the tree is open.
      leave_dirs_open = true, -- `false` closes auto expanded dirs, such as with `:Neotree reveal`
    }
  },
  buffers = {
    follow_current_file = {
      enabled = true, -- This will find and focus the file in the active buffer every time
      --              -- the current file is changed while the tree is open.
      leave_dirs_open = true, -- `false` closes auto expanded dirs, such as with `:Neotree reveal`
    },
  },
}

-- hide the statusline for this buffer
require("neo-tree.events").subscribe({
  event = "neo_tree_window_after_open",
  handler = function()
    vim.wo.statusline = ' '
  end,
})
