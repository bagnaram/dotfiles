# bagnaram's Neovim Setup

 ![bagnaram's neovim setup](nvim.webp "bagnaram's Neovim Setup") 

This is my neovim config. This config has been adapted from my vim config from a 
while back.

## Requirements
- Vim pathogen plugin by tpope (included in `bundle` directory) https://github.com/tpope/vim-pathogen
- Submodule  initilization `make nvim-pathogen`

## init.vim
This is the main configuration with separate submodule configs located in their 
respective `config` directory configuration files.


## Enjoy 😄
