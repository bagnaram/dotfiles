" Tab settings
set shiftwidth=2        " Tab spacing of 2 columns
set softtabstop=2       " unify
set shiftround          " round to nearest tab
set expandtab           " expand tabs to spaces

set cindent		
" smart indentation
"
set timeoutlen=500

" Dynamic incremental searching
set incsearch hlsearch
" syntax on

" --UI Config--
set number                "Show line numbers.  :set nonumber to turn off
set showcmd               "Show last vim command in bottom right of screen
set cursorline            "Highlight current line
set wildmenu              "Use autocomplete in command mode (using <TAB>)

let $PATH = $HOME . '/.local/share/mise/shims:' . $PATH

" Draw settings
set lazyredraw            "Redraw screen only when necessary
set showmatch             "Highlight matching brackets [{()}]

" VIM Pathogen plugin
execute pathogen#infect()

" Don't reset the cursor. Leave as block
set guicursor=
" Inherit colorscheme from terminal colors
" colo dim
colo catppuccin

" w - Trailing white space indicates a paragraph continues in the next line. A 
"     line that ends in a non-white character ends a paragraph.
" a - Automatic formatting of paragraphs. Every time text is inserted or
"     deleted the paragraph will be reformatted. When the 'c' flag is present 
"     this only happens for recognized comments.
" n - When formatting text, recognize numbered lists.
" t - auto-wrap.
set fo=wnt

" File specific commands
autocmd FileType terraform setlocal fo-=a fo-=c
autocmd FileType terraform nnoremap <buffer> <leader>tf :lcd %:p:h<CR>:!terraform fmt<CR>
autocmd BufRead,BufNewFile */templates/*.yaml,*/templates/*.tpl,*.gotmpl,helmfile*.yaml set ft=helm
" Use {{/* */}} as comments
autocmd FileType helm setlocal commentstring={{/*\ %s\ */}}
autocmd FileType markdown
            \ set comments=b:> |
            \ set formatlistpat=^\\s*\[-*+\]\\s
" Do Java autocomplete
autocmd Filetype java setlocal omnifunc=javacomplete#Complete
" magit-like rebase commands
autocmd Filetype NeogitRebaseTodo nnoremap <buffer> <silent> s :Squash<CR>
autocmd Filetype NeogitRebaseTodo nnoremap <buffer> <silent> p :Pick<CR>
autocmd Filetype NeogitRebaseTodo nnoremap <buffer> <silent> e :Edit<CR>
autocmd Filetype NeogitRebaseTodo nnoremap <buffer> <silent> r :Reword<CR>
autocmd Filetype NeogitRebaseTodo nnoremap <buffer> <silent> f :Fixup<CR>
autocmd Filetype NeogitRebaseTodo nnoremap <buffer> <silent> d :Drop<CR>
autocmd Filetype NeogitRebaseTodo setlocal noinsertmode
" test
" autocmd Filetype NeogitCommitMessage cmap <buffer> wq w!<CR><c-c><c-c>

" Read in day-night theme status
let scheme = readfile(expand('~/DAY'))[0]

" Change the color slightly for day/night cycle. Since PMenu is not part of
" the 16 terminal colors, define our own here.
if scheme == '0'
  set background=dark
"  hi Pmenu	cterm=none	ctermfg=255	ctermbg=235	guibg=#262626	guifg=#ffffff
  hi PmenuSel	cterm=none	ctermfg=255	ctermbg=21	guibg=#0000ff	guifg=#ffffff
  hi PmenuSbar	cterm=none	ctermfg=240	ctermbg=240	guibg=#444444
  hi PmenuThumb	cterm=none	ctermfg=255	ctermbg=255	guifg=#ffffff
endif
if scheme == '1'
  set background=light
  hi Pmenu	    cterm=none   ctermfg=16   ctermbg=252
  hi PmenuSel	    cterm=none   ctermfg=255  ctermbg=21
  hi PmenuSbar	    cterm=none   ctermfg=240  ctermbg=240
  hi PmenuThumb     cterm=none   ctermfg=255  ctermbg=255
  hi StatusLine     cterm=bold   ctermfg=8    ctermbg=23
  hi StatusLineMode cterm=bold   ctermfg=3    ctermbg=23
  hi NavicText      cterm=none   ctermfg=4    ctermbg=9
  hi NavicSeparator cterm=none   ctermfg=4    ctermbg=9
endif

"hi Visual         ctermbg=7 ctermfg=255
hi CursorLine cterm=NONE ctermbg=8 ctermfg=15
hi CursorLineNr cterm=bold ctermbg=8 ctermfg=15

hi NeogitSectionHeader ctermfg=4 cterm=bold
hi NeogitChangeModified ctermfg=Brown cterm=bold
hi NeogitChangeAdded ctermfg=22 cterm=bold
hi NeogitChangeDeleted ctermfg=88 cterm=bold

hi NeogitDiffAdd ctermfg=0 ctermbg=2 cterm=bold
hi NeogitDiffAddRegion ctermfg=0 ctermbg=2 cterm=bold
hi NeogitDiffDelete ctermfg=0 ctermbg=88 cterm=bold
hi NeogitDiffDeleteRegion ctermfg=0 ctermbg=88 cterm=bold
" hi NeogitDiffAddHighlight ctermfg=0 ctermbg=2 cterm=bold
" hi NeogitDiffDeleteHighlight ctermfg=0 ctermbg=88 cterm=bold
hi NeogitPopupSectionTitle ctermfg=4 cterm=bold
hi NeogitPopupSwitchKey ctermfg=5
hi NeogitPopupOptionKey ctermfg=5
hi NeogitPopupConfigKey ctermfg=5
hi NeogitPopupActionKey ctermfg=5
" hi link CursorLine NeogitCursorLine 

 
hi NeoTreeGitAdded ctermfg=Green
hi NeoTreeGitConflict ctermfg=Red
hi NeoTreeGitDeleted ctermfg=88
"hi NeoTreeGitIgnored ctermfg=
hi NeoTreeGitModified ctermfg=Brown
" hi NeoTreeGitUntracked

hi SignColumn ctermfg=none ctermbg=none


" Key bindings
let mapleader = " "
nnoremap <leader>bb :Telescope buffers show_all_buffers=true<CR>
nnoremap <leader>< :Telescope buffers show_all_buffers=true ignore_current_buffer=true sort_lastused=true<CR>
nnoremap <leader>bk :bp\|:bdelete #<CR>
nnoremap <leader>bl :buffers<CR>
nnoremap <leader>bs :call Scratch()<CR>
nnoremap <leader>gg :Neogit<CR>
nnoremap <leader>b[ :bprevious<CR>
nnoremap <leader>b] :bnext<CR>
nnoremap <leader>pp :Telescope projects<CR>
nnoremap <leader>op :Neotree toggle<CR>
" J keep it on same line
nnoremap J mzJ`z
nnoremap <PageUp> <PageUp>zz
nnoremap <PageDown> <PageDown>zz

nnoremap <leader>gl :GitLink<CR>
nnoremap <leader>gL :GitLink!<CR>
vnoremap <leader>gl :GitLink<CR>
vnoremap <leader>gL :GitLink!<CR>

" paste in visual preserve copy buffer
xnoremap <Leader>p "_dP

" copy into system buffer
xnoremap <leader>y "+y
nnoremap <leader>Y "+Y

" Load all the LUA config files
runtime config/telescope.lua
runtime config/which-key.lua
runtime config/icons.lua
runtime config/neogit.lua
runtime config/neotree.lua
runtime config/treesitter.lua
runtime config/lsp.lua
runtime config/project.lua
runtime config/gitlinker.lua
runtime config/catpuccin.lua

" Inline LUA config
lua <<EOF

-- local bg_color = vim.api.nvim_get_option('background')
-- print('Background color: ' .. bg_color)
-- local bg_color = vim.api.nvim_get_hl_by_name("Title", true)

-- print the color value
-- vim.pretty_print(bg_color)
-- local c = print(bg_color[true])
-- local function clamp(component)
--   return math.min(math.max(component, 0), 255)
-- end
-- function LightenDarkenColor(col, amt)
--   local num = tonumber(col, 16)
--   local r = math.floor(num / 0x10000) + amt
--   local g = (math.floor(num / 0x100) % 0x100) + amt
--   local b = (num % 0x100) + amt
--   return string.format("%#x", clamp(r) * 0x10000 + clamp(g) * 0x100 + clamp(b))
-- end

vim.keymap.set(
  {"n", 'v'},
  "<leader>gl",
  "<cmd>GitLink<cr>",
  { silent = true, noremap = true, desc = "Copy git permlink to clipboard" }
)
vim.keymap.set(
  {"n", 'v'},
  "<leader>gL",
  "<cmd>GitLink!<cr>",
  { silent = true, noremap = true, desc = "Open git permlink in browser" }
)
-- blame
vim.keymap.set(
  {"n", 'v'},
  "<leader>gb",
  "<cmd>GitLink blame<cr>",
  { silent = true, noremap = true, desc = "Copy git blame link to clipboard" }
)
vim.keymap.set(
  {"n", 'v'},
  "<leader>gB",
  "<cmd>GitLink! blame<cr>",
  { silent = true, noremap = true, desc = "Open git blame link in browser" }
)

vim.keymap.set("v", "<leader>ff", vim.lsp.buf.format, { remap = false })

require'colorizer'.setup()

EOF

" Function to create new scratch buffer
function! Scratch()
  enew
  setlocal buftype=nofile bufhidden=hide noswapfile
  setlocal filetype=scratch
endfunction


" Vanilla statusline setup
set statusline=
" show full file path
set statusline+=%<%f
" show full file path
set statusline+=%#StatusLine#
set statusline+=\ %{%v:lua.require'nvim-navic'.get_location()%}
" show line mode
set statusline+=\ %h%m%r%=%-14.(%l,%c%V%)\ %P
" show current mode
set statusline+=%#StatusLineMode#%{(mode()=='n')?'\ \ NORMAL\ ':''}
set statusline+=%#StatusLineMode#%{(mode()=='i')?'\ \ INSERT\ ':''}
set statusline+=%#StatusLineMode#%{(mode()=='R')?'\ \ REPLACE\ ':''}
set statusline+=%#StatusLineMode#%{(mode()=='v')?'\ \ VISUAL\ ':''}
